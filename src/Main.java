import java.util.Scanner;

class Main
{
    public static void main(String args[]) {
        double taxAmount=0;
        Scanner userInput=new Scanner(System.in);
        System.out.print("Enter the income for which tax is to be calculated: ");
        double income=userInput.nextDouble();

        System.out.println("The income tax for "+income+" is: ");

        if(income>500000) {
            taxAmount=0.06*(income-500000);
            System.out.println("For "+(income-500000)+" Tax Amount ="+taxAmount); income=500000;
        }
        if(income>250000 && income <=500000) {
            taxAmount=taxAmount+0.05*(income-250000);
            System.out.println("For "+(income-250000)+" Tax Amount ="+0.05*(income-250000)); income=250000;
        }
        if(income>100000 && income <=250000) {
            taxAmount=taxAmount+0.04*(income-100000);
            System.out.println("For "+(income-100000)+" Tax Amount ="+0.04*(income-100000)); income=100000;
        }
        if(income>75000 && income <=100000) {
            taxAmount=taxAmount+0.03*(income-75000);
            System.out.println("For "+(income-75000)+" Tax Amount ="+0.03*(income-75000)); income=75000;
        }
        if(income>50000 && income <=75000) {
            taxAmount=taxAmount+0.02*(income-50000);
            System.out.println("For "+(income-50000)+" Tax Amount ="+0.02*(income-50000)); income=50000;
        }
        if(income<=50000) {
            taxAmount=taxAmount+0.01*income;
            System.out.println("For "+(income)+" Tax Amount ="+0.01*income);
        }
        System.out.println("Total Tax Amount= "+taxAmount);
    }
}